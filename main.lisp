(defun menu()
    (format t "~%Hello! Select:~%")
    (format t "1 - show all books~%");+
    (format t "2 - add book~%");+
    (format t "3 - delete book~%");+
    (format t "4 - get a recommendation~%");+
    (format t "5 - book search by cost~%");+
    (format t "6 - book search by date~%");+
    (format t "7 - book search by book name~%");+
    (format t "8 - book search by author~%");+
    (format t "9 - book search by style~%");+
    (format t "10 - sort by cost~%");+
    (format t "11 - sort by date~%");+
    (format t "0 - exit~%");+
    (read))

(defun exst_indexes (table)
    (setq indeces (loop for x in (cdr table)
        collect (parse-integer (car x))))
    indeces)

(defun max_index (table)
    (reduce #'max (exst_indexes table)))

(defun find_styles (index table)
    (setq style_list 
        (loop for x in (cdr table)
            collect (if  (= index (parse-integer (car x)))
                (car (cdr x)))))
    (remove-if #'null style_list))

(defun show_all_data (books styles)
    (loop for x in (cdr books)
        do (setq ind (parse-integer (car x)))
        (print (append x (find_styles ind styles)) )))

(defun show_all_data_by_ind (target_i)
    (loop for x in (cdr books)
        do (setq ind (parse-integer (car x)))
        (if (= ind target_i) (print (append x (find_styles ind styles)) ))))

(defun prep_book_row (books styles)
    (format t "~%Enter book name: ") (setq name (string (read)))
    (format t "~%Enter data: ") (setq data (string (read)))
    (format t "~%Enter book cost: ") (setq cost (write-to-string (read)))
    (format t "~%Enter book author: ") (setq author (string (read-line)))
    (format t "~%Enter book storage: ") (setq storage (string (read)))
    (format t "~%Enter styles separated by spaces: ") (setq styles_string (read-line))

    (setq ind (write-to-string  (+ 1 (max_index books))))
    (setq book_row (list ind name data cost author storage))
    (setq styles_list (cl-ppcre:split "\\s+" styles_string))
    (setq style_row (loop for x in styles_list 
                        collect (list ind x)))
    (list book_row style_row))

(defun is-book (row)
    (= the_index (parse-integer (car row))))
(defun remove-book (index)
    (defvar the_index index)
    (setq books (remove-if #'is-book (cdr books)))
    (setq styles (remove-if #'is-book (cdr styles))))

(defun select-by-style (name)
    (setq style_list 
        (loop for x in (cdr styles)
            collect (if  (string= name (cadr x))
                (parse-integer (car x)))))
    (remove-if #'null style_list))

(defun nshuffle (sequence)
  (loop for i from (length sequence) downto 2
        do (rotatef (elt sequence (random i))
                    (elt sequence (1- i))))
  sequence)

(defun get_cost (row) (parse-integer (cadddr row)))
(defun sort_by_cost (temp)
    ;0 - in increasing order
    ;1 - in decreasing order
    (if (= temp 0)
        (setq sorted_books (sort (copy-list (cdr books)) #'< :key #'get_cost))
        (setq sorted_books (sort (copy-list (cdr books)) #'> :key #'get_cost)))
    
    (show_all_data sorted_books styles))

(defun date_to_days (date) 
    (setq temp (cl-ppcre:split "\\." date))
    (setq days (parse-integer (car temp)))
    (setq moun (* 31 (parse-integer (cadr temp))))
    (setq years (* 365 (parse-integer (car (last temp)))))
    (+ days moun years))
(defun get_date (row) (date_to_days (caddr row)))
(defun sort_by_date (temp)
    ;0 - in increasing order
    ;1 - in decreasing order
    (if (= temp 0)
        (setq sorted_books (sort (copy-list (cdr books)) #'< :key #'get_date))
        (setq sorted_books (sort (copy-list (cdr books)) #'> :key #'get_date)))
    
    (show_all_data sorted_books styles))

(defun show_all_data_by_cost (min_cost max_cost)
    (loop for x in (cdr books) do
        (setq cost (parse-integer (cadddr x)))
        (setq ind (parse-integer (car x)))
        (if (and (> cost min_cost) (< cost max_cost)) (print (append x (find_styles ind styles)) ))))

(defun show_all_data_by_date (date_min_str date_max_str)
    (setq date_min (date_to_days date_min_str))
    (setq date_max (date_to_days date_max_str))
    (loop for x in (cdr books) do
        (setq cur_date (date_to_days (caddr x)))
        (setq ind (parse-integer (car x)))
        (if (and (> cur_date date_min) (< cur_date date_max)) (print (append x (find_styles ind styles)) ))))

(defun select_by_style ()
    (format t "~%enter style name (example: novel, horror): ")
    (setq style_name (string-downcase (string (read))))
    (format t "~%Selected books: ")
    (setq styles_list (select-by-style style_name))
    (loop for ind in style_list do
        (if (not (eq nil ind)) (show_all_data_by_ind ind)) ))

(defun show_all_data_by_book_name (target_name)
    (loop for x in (cdr books) do
        (setq book_name (string-downcase (cadr x)))
        (setq ind (parse-integer (car x)))
        (if (not (eq (search target_name book_name) nil)) (print (append x (find_styles ind styles)) ))))

(defun show_all_data_by_author (target_author)
    (loop for x in (cdr books) do
        (setq author (string-downcase (cadddr (cdr x))))
        (setq ind (parse-integer (car x)))
        (if (not (eq (search target_author author) nil)) (print (append x (find_styles ind styles)) ))))

(defun main ()
    (defvar books (cl-csv:read-csv #P".\\database\\Books.csv"))
    (defvar styles (cl-csv:read-csv #P".\\database\\Styles.csv"))
    (defvar choose (menu))
    ;show all books
    (cond 
    ((= choose 1) (show_all_data books styles) (makunbound 'choose)(main))

    ;add book
    ((= choose 2)
    (setq answ (prep_book_row books styles))
    (setq books (append books (list (car answ))))
    (setq styles (append styles (car (cdr answ))))
    (makunbound 'choose)(main))

    ;remove book
    ((= choose 3)
    (format t "~%Enter index hate book: ")
    (setq index (read))
    (remove-book index)
    (makunbound 'choose)(main))

    ;recommendation
    ((= choose 4)
    (format t "~%enter style name (example: novel, horror): ")
    (setq style_name (string-downcase (string (read))))
    (format t "~%We recommend you read this book: ")
    (show_all_data_by_ind (car (nshuffle (select-by-style style_name))))
    (makunbound 'choose)(main))

    ; book search by cost
    ((= choose 5)
    (format t "~%Enter min price: ")
    (setq min_cost (read))
    (format t "~%Enter max price: ")
    (setq max_cost (read))
    (show_all_data_by_cost min_cost max_cost)
    (makunbound 'choose)(main))

    ;book search by date
    ((= choose 6)
    (format t "~%date format is as 09.06.2012")
    (format t "~%Enter min date: ")
    (setq min_date (string (read)))
    (format t "~%Enter max date: ")
    (setq max_date (string (read)))
    (show_all_data_by_date min_date max_date)
    (makunbound 'choose)(main))

    ;book search by book name
    ((= choose 7)
    (format t "~%Enter book name: ")
    (setq book_name (string-downcase (string (read))))
    (show_all_data_by_book_name book_name)
    (makunbound 'choose)(main))

    ;book search by author
    ((= choose 8)
    (format t "~%Enter author name: ")
    (setq author (string-downcase (string (read))))
    (show_all_data_by_author author)
    (makunbound 'choose)(main))

    ;book search by style
    ((= choose 9)
    (select_by_style)
    (makunbound 'choose)(main))

    ;sort by cost
    ((= choose 10)
    (format t "~%0 - in increasing order~%1 - in decreasing order~%")
    (sort_by_cost (read))
    (makunbound 'choose)(main))
    
    ;sort by date
    ((= choose 11)
    (format t "~%0 - in increasing order~%1 - in decreasing order~%")
    (sort_by_date (read))
    (makunbound 'choose)(main))


    (t (format t "~%Goodbye!~%")))
)